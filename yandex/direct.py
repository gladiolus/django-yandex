from extdirect.django import ExtRemotingProviderBorg, ExtDirectStore
from extdirect.django.decorators import remoting

from yandex.models import YandexForecastItem, YandexRegion


remote_provider = ExtRemotingProviderBorg()


@remoting(remote_provider, 'YandexRegion', len=1, name='read')
def yandex_region_read(request):
    params = request.extdirect_post_data[0]

    getters = [
        ('text', lambda x: x.display_name),
        ('leaf', lambda x: not x.has_children),
        ('parent', lambda x: x.parent_id),
        ('checked', lambda x: False)
    ]

    exclude = ['parent']

    store = ExtDirectStore(YandexRegion, metadata=False, extras=getters,
                           extra_fields=[name for name, g in getters], exclude_fields=exclude)
    qs = YandexRegion.objects.all()

    return store.query(qs, **params)
