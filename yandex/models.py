import re
from datetime import datetime
import json
import hashlib
import requests

from django.conf import settings
from django.db import models
from django.db.models.signals import post_save
from django.utils.translation import ugettext_lazy as _
from django.template import Context, loader

from . import utils


class YandexRegion(models.Model):
    id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=255)
    name_ru = models.CharField(max_length=255)
    parent = models.ForeignKey('self', related_name='children', null=True, blank=True)
    type = models.CharField(max_length=30, db_index=True, blank=True)

    @property
    def display_name(self):
        return self.name_ru if self.name_ru else self.name

    @property
    def has_children(self):
        return self.children.exists()

    def __unicode__(self):
        return self.display_name


class YandexRubric(models.Model):
    id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=255)
    name_ru = models.CharField(max_length=255)
    full_name = models.CharField(max_length=255)
    full_name_ru = models.CharField(max_length=255)
    parent = models.ForeignKey('self', related_name='children', null=True, blank=True)
    url = models.CharField(max_length=255)
    checkable = models.BooleanField(default=False)

    def __unicode__(self):
        return u'{}'.format(self.name)


class Phrase(models.Model):
    created = models.DateTimeField(auto_now_add=True)

    text = models.TextField()
    phrase = models.TextField(blank=True)
    hash = models.CharField(max_length=32, blank=True)

    related = models.ManyToManyField('self', through='PhraseRelation',
                                     symmetrical=False,
                                     through_fields=('phrase_from', 'phrase_to'),
                                     related_name='+')
    @property
    def forecast(self):
        if not hasattr(self, '_cached_forecast'):
            try:
                forecast = self.forecasts.select_related('region').latest()
            except YandexForecastItem.DoesNotExist:
                forecast = None

            self._cached_forecast = forecast

        return self._cached_forecast

    @property
    def has_related(self):
        return self.related.exists()

    @classmethod
    def normalize(cls, phrase, reorder=True):
        invalid_symbols = re.compile('[\W\s_]+', re.U)
        phrase = re.sub(invalid_symbols, u' ', phrase).strip()
        return u" ".join(sorted(phrase.split())) if reorder else phrase

    @classmethod
    def calc_hash(cls, phrase):
        normalized = cls.normalize(phrase)
        return hashlib.md5(normalized.encode('utf-8')).hexdigest()

    @classmethod
    def bulk_get_or_create(cls, raw_phrases):
        result = []
        phrases = {Phrase.calc_hash(text): text for text in raw_phrases}

        for hashed, text in phrases.items():
            data = dict(text=text, phrase=Phrase.normalize(text))
            try:
                phrase, _ = Phrase.objects.get_or_create(hash=hashed, defaults=data)
            except Phrase.MultipleObjectsReturned:
                phrase = Phrase.objects.filter(hash=hashed)[:1].get()

            result.append(phrase)

        return result

    @classmethod
    def create_phrases(cls, phrases):
        if isinstance(phrases, basestring):
            phrases = utils.split_yandex_phrases(phrases)

        raw = {Phrase.calc_hash(p): p for p in phrases}
        old = Phrase.objects.filter(hash__in=raw.keys())
        new = []

        for p in set(raw.keys()).difference(set(old.values_list('hash', flat=True))):
            t = raw[p]
            new.append(Phrase(text=t, phrase=Phrase.normalize(t), hash=Phrase.calc_hash(t)))
        Phrase.objects.bulk_create(new)

        return Phrase.objects.filter(hash__in=raw)


    def __unicode__(self):
        return u'{}'.format(self.phrase)

    def save(self, *args, **kwargs):
        if self.id is None:
            self.phrase = self.normalize(self.text)
            self.hash = self.calc_hash(self.text)

        super(Phrase, self).save(*args, **kwargs)


PHRASE_RELATION_TYPE = (
    ('search_also', _('Search also')),
    ('search_with', _('Search with')),
    ('suggestion', _('Suggestion')),
)


class PhraseRelation(models.Model):
    type = models.CharField(max_length=15, choices=PHRASE_RELATION_TYPE, db_index=True)
    created = models.DateTimeField(auto_now_add=True)
    phrase_from = models.ForeignKey(Phrase, related_name='rel_from')
    phrase_to = models.ForeignKey(Phrase, related_name='rel_to')

    def __unicode__(self):
        return u'{} - {} ({})'.format(self.phrase_from.text, self.phrase_to.text, self.type)


class YandexXMLRequest(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    phrase = models.ForeignKey(Phrase)
    region = models.ForeignKey(YandexRegion, null=True)

    def __unicode__(self):
        return u'{} - {}'.format(self.phrase, self.created)


class YandexXMLRequestPage(models.Model):
    request = models.ForeignKey(YandexXMLRequest, related_name='pages')
    page = models.IntegerField(default=0)
    data = models.TextField(blank=True)

    def __unicode__(self):
        return u'{} {}'.format(self.request, self.page)


class YandexWordstatReport(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    external = models.IntegerField(default=0)
    phrases = models.ManyToManyField(Phrase)
    region = models.ForeignKey(YandexRegion, null=True, blank=True)

    def make_request(self, countdown=settings.YANDEX_COUNTDOWN):
        from . import tasks
        tasks.do_yandex_wordstat_report.apply_async((self.pk,), countdown=countdown)


class YandexWordstatReportData(models.Model):
    report = models.OneToOneField(YandexWordstatReport, related_name='data')
    data = models.TextField()


class YandexForecastReport(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    external = models.IntegerField(default=0)
    phrases = models.ManyToManyField(Phrase)
    region = models.ForeignKey(YandexRegion, null=True, blank=True)

    def make_request(self, countdown=settings.YANDEX_COUNTDOWN):
        from . import tasks
        tasks.do_yandex_forecast_report.apply_async((self.pk,), countdown=countdown)


class YandexForecastReportData(models.Model):
    report = models.OneToOneField(YandexForecastReport, related_name='data')
    data = models.TextField()


class YandexForecastItem(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    phrase = models.ForeignKey(Phrase, related_name='forecasts')
    report = models.ForeignKey(YandexForecastReport)
    region = models.ForeignKey(YandexRegion)

    min = models.FloatField()
    max = models.FloatField()
    premium_min = models.FloatField()
    premium_max = models.FloatField()
    shows = models.IntegerField()
    clicks = models.IntegerField()
    first_place_clicks = models.IntegerField()
    premium_clicks = models.IntegerField()
    ctr = models.FloatField()
    first_place_ctr = models.FloatField()
    premium_ctr = models.FloatField()

    class Meta:
        get_latest_by = 'created'


class LatestYandexForecastItem(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    phrase = models.ForeignKey(Phrase, related_name='latest_forecasts')
    report = models.ForeignKey(YandexForecastReport)
    region = models.ForeignKey(YandexRegion)

    min = models.FloatField()
    max = models.FloatField()
    premium_min = models.FloatField()
    premium_max = models.FloatField()
    shows = models.IntegerField()
    clicks = models.IntegerField()
    first_place_clicks = models.IntegerField()
    premium_clicks = models.IntegerField()
    ctr = models.FloatField()
    first_place_ctr = models.FloatField()
    premium_ctr = models.FloatField()

    class Meta:
        unique_together = ('phrase', 'region')


class YandexDirectPhrase(models.Model):
    external = models.BigIntegerField(unique=True)
    phrase = models.ForeignKey(Phrase)

    raw = models.TextField()

    def __unicode__(self):
        return u'{} ({})'.format(self.phrase.text, self.external)


class YandexDirectBanner(models.Model):
    external = models.BigIntegerField(unique=True, default=0)
    is_active = models.BooleanField(default=False)
    status_show = models.BooleanField(default=False)
    domain = models.CharField(max_length=255)
    href = models.TextField()
    title = models.CharField(max_length=33)
    text = models.TextField()

    raw = models.TextField()

    campaign = models.ForeignKey('YandexDirectCampaign', related_name='banners')
    phrases = models.ManyToManyField(YandexDirectPhrase, related_name='banners')
    regions = models.ManyToManyField(YandexRegion, related_name='banners')

    templates = models.ManyToManyField('xtemplate.Template', related_name='banners',
                                       through='YandexDirectBannerTemplateRelation')
    links = models.ManyToManyField('xtemplate.Link', related_name='banners')

    def __unicode__(self):
        return u'{} ({})'.format(self.title, self.external)


class YandexDirectBannerTemplateRelation(models.Model):
    banner = models.ForeignKey(YandexDirectBanner, related_name='+')
    template = models.ForeignKey('xtemplate.Template', related_name='+')
    field_name = models.CharField(max_length=255)


class YandexDirectCampaign(models.Model):
    external = models.BigIntegerField(unique=True, default=0)
    login = models.CharField(max_length=255)
    name = models.CharField(max_length=255)
    start_date = models.DateTimeField()
    is_active = models.BooleanField(default=False)

    raw = models.TextField()

    @classmethod
    def fetch_campaigns(cls):
        r = utils.do_yandex_direct_request('GetCampaignsList',
                                           param=[settings.YANDEX_DIRECT_CLIENT_LOGIN])
        if r.status_code == requests.codes.ok:
            response = json.loads(r.content)
            data = response.get('data')
            if data:
                for c in data:
                    cls.objects.get_or_create(external=c.get('CampaignID'), defaults={
                        'login': c.get('Login'),
                        'name': c.get('Name'),
                        'start_date': datetime.strptime(c.get('StartDate'), '%Y-%m-%d'),
                        'is_active': True if c.get('IsActive') == 'Yes' else False,
                        'raw': json.dumps(c)
                    })

    @classmethod
    def create_campaign(cls, params):
        defaults = json.loads(loader.get_template('search_campaign.json').render(Context()))
        params = dict(utils.merge_dicts(defaults, params))

        r = utils.do_yandex_direct_request('CreateOrUpdateCampaign', params)
        if r.status_code == requests.codes.ok:
            campaign_id = r.json().get('data')
            if campaign_id:
                campaign = cls(external=campaign_id)
                campaign.fetch_params()
                return campaign

    def __unicode__(self):
        return u'{} ({})'.format(self.name, self.external)

    def archive(self):
        r = utils.do_yandex_direct_request('ArchiveCampaign', param={'CampaignID': self.external})

    def unarchive(self):
        r = utils.do_yandex_direct_request('UnArchiveCampaign', param={'CampaignID': self.external})

    def fetch_banners(self):
        r = utils.do_yandex_direct_request('GetBanners', param={'CampaignIDS': [self.external]})
        if r.status_code == requests.codes.ok:
            response = json.loads(r.content)
            data = response.get('data')
            if data:
                for b in data:
                    banner, created = YandexDirectBanner.objects.get_or_create(external=b.get('BannerID'), defaults={
                        'is_active': True if b.get('IsActive') == 'Yes' else False,
                        'status_show': True if b.get('StatusShow') == 'Yes' else False,
                        'domain': b.get('Domain'),
                        'href': b.get('Href'),
                        'title': b.get('Title'),
                        'text': b.get('Text'),
                        'raw': json.dumps(b),
                        'campaign': self
                    })
                    for p in b.get('Phrases'):
                        phrase, created = Phrase.objects.get_or_create(hash=Phrase.calc_hash(p.get('Phrase')), defaults={
                            'text': p.get('Phrase')
                        })
                        direct_phrase, created = YandexDirectPhrase.objects.get_or_create(external=p.get('PhraseID'), defaults={
                            'phrase': phrase,
                            'raw': json.dumps(p)
                        })
                        banner.phrases.add(direct_phrase)

    def fetch_params(self):
        r = utils.do_yandex_direct_request('GetCampaignsParams', param={'CampaignIDS': [self.external]})
        if r.status_code == requests.codes.ok:
            response = json.loads(r.content)
            data = response.get('data')
            if data:
                c = data[0]
                self.login = c.get('Login')
                self.name = c.get('Name')
                self.start_date = datetime.strptime(c.get('StartDate'), '%Y-%m-%d')
                self.is_active = True if c.get('IsActive') == 'Yes' else False
                self.raw = json.dumps(c)
                self.save()

    def resume(self):
        r = utils.do_yandex_direct_request('ResumeCampaign', param={'CampaignID': self.external})

    def stop(self):
        r = utils.do_yandex_direct_request('StopCampaign', param={'CampaignID': self.external})


class YandexDirectReport(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    campaign = models.ForeignKey(YandexDirectCampaign, related_name='reports')
    start_date = models.DateField()
    end_date = models.DateField()

    data = models.TextField(blank=True)

    def make_request(self):
        param = {
            'CampaignID': self.campaign.external,
            'StartDate': self.start_date.strftime('%Y-%m-%d'),
            'EndDate': self.end_date.strftime('%Y-%m-%d'),
            'GroupByColumns': ['clPhrase'],
        }
        r = utils.do_yandex_direct_request('GetBannersStat', param)
        if r.status_code == requests.codes.ok:
            data = json.loads(r.content)
            if data.get('data'):
                self.data = r.content
                self.save()
                for s in data.get('data').get('Stat'):
                    campaign = self.campaign
                    banner = YandexDirectBanner.objects.get(external=s.get('BannerID'))
                    phrase = YandexDirectPhrase.objects.get(external=s.get('PhraseID'))
                    defaults={
                        'clicks': s.get('Clicks'),
                        'shows': s.get('Shows'),
                        'sum': s.get('Sum'),
                        'clicks_search': s.get('ClicksSearch'),
                        'shows_search': s.get('ShowsSearch'),
                        'sum_search': s.get('SumSearch'),
                        'clicks_context': s.get('ClicksContext'),
                        'shows_context': s.get('ShowsContext'),
                        'sum_context': s.get('SumContext'),
                    }
                    item, created = YandexDirectItem.objects.get_or_create(
                        report=self,
                        campaign=campaign,
                        banner=banner,
                        phrase=phrase,
                        defaults=defaults,
                    )
                    if not created:
                        YandexDirectItem.objects.filter(pk=item.pk).update(**defaults)


class YandexDirectItem(models.Model):
    report = models.ForeignKey(YandexDirectReport)
    campaign = models.ForeignKey(YandexDirectCampaign)
    banner = models.ForeignKey(YandexDirectBanner)
    phrase = models.ForeignKey(YandexDirectPhrase)
    clicks = models.IntegerField()
    shows = models.IntegerField()
    sum = models.DecimalField(max_digits=14, decimal_places=2)
    clicks_search = models.IntegerField()
    shows_search = models.IntegerField()
    sum_search = models.DecimalField(max_digits=14, decimal_places=2)
    clicks_context = models.IntegerField()
    shows_context = models.IntegerField()
    sum_context = models.DecimalField(max_digits=14, decimal_places=2)


def parse_yandex_wordstat_report_data(signal, sender, instance, created, **kwargs):
    if not created or kwargs.get('raw'):
        return

    data = json.loads(instance.data)

    raw_phrases = {}
    searched_with = {}
    searched_also = {}

    for d in data['data']:
        phrase_from = Phrase.calc_hash(d['Phrase'])

        if d.get('SearchedWith'):
            searched_with[phrase_from] = []
            for p in d['SearchedWith']:
                h = Phrase.calc_hash(p['Phrase'])
                raw_phrases[h] = p['Phrase']
                searched_with[phrase_from].append(h)

        if d.get('SearchedAlso'):
            searched_also[phrase_from] = []
            for p in d['SearchedAlso']:
                h = Phrase.calc_hash(p['Phrase'])
                raw_phrases[h] = p['Phrase']
                searched_also[phrase_from].append(h)

    qs = Phrase.create_phrases(raw_phrases.values())
    phrases = dict(qs.values_list('hash', 'id'))

    relations = []
    for k, v in searched_with.items():
        for p in v:
            relations.append(PhraseRelation(phrase_from_id=phrases[k], phrase_to_id=phrases[p],
                                            type='search_with'))

    for k, v in searched_also.items():
        for p in v:
            relations.append(PhraseRelation(phrase_from_id=phrases[k], phrase_to_id=phrases[p],
                                            type='search_also'))

    PhraseRelation.objects.bulk_create(relations)
    utils.create_yandex_forecast_report(qs, instance.report.region)
    #utils.create_yandex_xml_requests(qs, instance.report.region)


def parse_yandex_forcast_report_data(signal, sender, instance, created, **kwargs):
    if not created or kwargs.get('raw'):
        return

    data = json.loads(instance.data)
    bulk = []
    latest = []
    for d in data['data']['Phrases']:
        phrase = Phrase.objects.filter(hash=Phrase.calc_hash(d['Phrase']))[0]
        region = instance.report.region
        data = {
            'report': instance.report,
            'min': d['Min'],
            'max': d['Max'],
            'premium_min': d['PremiumMin'],
            'premium_max': d['PremiumMax'],
            'shows': d['Shows'],
            'clicks': d['Clicks'],
            'first_place_clicks': d['FirstPlaceClicks'],
            'premium_clicks': d['PremiumClicks'],
            'ctr': d['CTR'],
            'first_place_ctr': d['FirstPlaceCTR'],
            'premium_ctr': d['PremiumCTR'],
        }
        bulk.append(YandexForecastItem(phrase=phrase, region=region, **data))
        latest.append((phrase, region, data))

    YandexForecastItem.objects.bulk_create(bulk)
    for phrase, region, data in latest:
        LatestYandexForecastItem.objects.update_or_create(phrase=phrase, region=region, defaults=data)


post_save.connect(parse_yandex_wordstat_report_data, sender=YandexWordstatReportData)
post_save.connect(parse_yandex_forcast_report_data, sender=YandexForecastReportData)
