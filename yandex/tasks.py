from datetime import datetime
from celery import shared_task
import json
import requests
from pyquery import PyQuery
import logging

from django.conf import settings

from . import models
from .utils import do_yandex_direct_request


logger = logging.getLogger(__name__)


@shared_task
def do_yandex_xml_request(request_pk, countdown=0):
    xml_request = models.YandexXMLRequest.objects.get(pk=request_pk)

    params = {
        'query': xml_request.phrase.text,
        'user': settings.YANDEX_XML_USER,
        'key': settings.YANDEX_XML_KEY,
        'page': 0,
    }
    if xml_request.region:
        params['lr'] = xml_request.region.pk

    for page in range(settings.YANDEX_XML_MAX_PAGES):
        params['page'] = page
        do_yandex_xml_request_page.apply_async((request_pk, params), countdown=countdown+page)
    return xml_request.pk


@shared_task
def do_yandex_xml_request_page(request_pk, params):
    proxies = settings.YANDEX_XML_PROXIES
    timeout = settings.YANDEX_TIMEOUT
    url = settings.YANDEX_XML_URL
    request = models.YandexXMLRequest.objects.get(pk=request_pk)

    response = requests.get(url, params=params, proxies=proxies, timeout=timeout)
    if response.status_code == 200:
        pq = PyQuery(response.content)
        if pq('error') and pq('error').attr('code'):
            logger.error("Yandex XML error code=%s, query='%s': %s",
                         pq('error').attr('code'), params['query'], pq('error').text())
        else:
            models.YandexXMLRequestPage(request=request, page=params.get('page'),
                                        data=response.text).save()


@shared_task
def do_yandex_wordstat_report(report_pk):
    report = models.YandexWordstatReport.objects.get(pk=report_pk)
    phrases = list(report.phrases.values_list('text', flat=True))
    param = dict(Phrases=phrases)
    if report.region_id:
        param['GeoID'] = [report.region_id]

    try:
        r = do_yandex_direct_request('CreateNewWordstatReport', param, raise_on_errors=True)
    except requests.exceptions.RequestException as e:
        retries = None if getattr(e, 'yandex_queue_is_full', False) else 3
        self.retry(exc=e, countdown=2 * settings.YANDEX_COUNTDOWN, max_retries=retries)
    else:
        report.external = r.json().get('data')
        report.save()
        fetch_yandex_wordstat_report.apply_async((report_pk,), countdown=settings.YANDEX_COUNTDOWN)

    return report_pk


@shared_task
def fetch_yandex_wordstat_report(report_pk):
    report = models.YandexWordstatReport.objects.get(pk=report_pk)
    if report.external:
        try:
            r = do_yandex_direct_request('GetWordstatReport', report.external, raise_on_errors=True)
        except requests.exceptions.RequestException as e:
            self.retry(exc=e, countdown=2 * settings.YANDEX_COUNTDOWN, max_retries=3)
        else:
            report_data = models.YandexWordstatReportData(report=report, data=r.content)
            report_data.save()
            do_yandex_direct_request('DeleteWordstatReport', report.external)

    return report_pk


@shared_task
def clean_yandex_wordstat_reports():
    r = do_yandex_direct_request('GetWordstatReportList')
    deleted_count = 0

    for report in r.json().get('data', []):
        if report.get('StatusReport') == 'Done':
            r = do_yandex_direct_request('DeleteWordstatReport', report.get('ReportID'))
            if r.json().get('data') == 1:
                deleted_count += 1

    return deleted_count


@shared_task(bind=True)
def do_yandex_forecast_report(self, report_pk):
    report = models.YandexForecastReport.objects.get(pk=report_pk)
    phrases = list(report.phrases.values_list('text', flat=True))
    param = dict(Phrases=phrases)
    if report.region_id:
        param['GeoID'] = [report.region_id]

    try:
        r = do_yandex_direct_request('CreateNewForecast', param, raise_on_errors=True)
    except requests.exceptions.RequestException as e:
        retries = None if getattr(e, 'yandex_queue_is_full', False) else 3
        self.retry(exc=e, countdown=2 * settings.YANDEX_COUNTDOWN, max_retries=retries)
    else:
        report.external = r.json().get('data')
        report.save()
        fetch_yandex_forecast_report.apply_async((report_pk,), countdown=settings.YANDEX_COUNTDOWN)

    return report_pk


@shared_task(bind=True)
def fetch_yandex_forecast_report(self, report_pk):
    report = models.YandexForecastReport.objects.get(pk=report_pk)
    if report.external:
        try:
            r = do_yandex_direct_request('GetForecast', report.external, raise_on_errors=True)
        except requests.exceptions.RequestException as e:
            self.retry(exc=e, countdown=2 * settings.YANDEX_COUNTDOWN, max_retries=3)
        else:
            report_data = models.YandexForecastReportData(report=report, data=r.content)
            report_data.save()

            do_yandex_direct_request('DeleteForecastReport', report.external, raise_on_errors=True)

    return report_pk


@shared_task
def clean_yandex_forecast_reports():
    r = do_yandex_direct_request('GetForecastList')
    deleted_count = 0

    for report in r.json().get('data', []):
        if report.get('StatusForecast') == 'Done':
            r = do_yandex_direct_request('DeleteForecastReport', report.get('ForecastID'))
            if r.json().get('data') == 1:
                deleted_count += 1

    return deleted_count


@shared_task
def do_yandex_direct_report(report_pk):
    report = models.YandexDirectReport.objects.get(pk=report_pk)
    param = {
        'CampaignID': report.campaign.external,
        'StartDate': report.start_date.strftime('%Y-%m-%d'),
        'EndDate': report.end_date.strftime('%Y-%m-%d'),
    }
    r = do_yandex_direct_request('CreateNewReport', param)
    if r.status_code == requests.codes.ok:
        response = json.loads(r.content)
        if response.get('data'):
            report.external = response.get('data')
            report.save()
            fetch_yandex_direct_report.apply_async((report_pk,), countdown=settings.YANDEX_COUNTDOWN)
    return report_pk


@shared_task
def fetch_yandex_direct_report(report_pk):
    report = models.YandexDirectReport.objects.get(pk=report_pk)
    if report.external:
        r = do_yandex_direct_request('GetDirect', report.external)
        if r.status_code == requests.codes.ok:
            response = json.loads(r.content)
            if response.get('data'):
                report_data = models.YandexDirectReportData(report=report, data=r.content)
                report_data.save()
                do_yandex_direct_request('DeleteDirectReport', report.external)
    return report_pk


@shared_task
def clean_yandex_direct_reports():
    r = do_yandex_direct_request('GetDirectList')
    deleted_count = 0

    for report in r.json().get('data', []):
        if report.get('StatusDirect') == 'Done':
            r = do_yandex_direct_request('DeleteDirectReport', report.get('DirectID'))
            if r.json().get('data') == 1:
                deleted_count += 1

    return deleted_count


@shared_task
def fetch_daily_direct_reports(date=None):
    Report = models.YandexDirectReport

    if date is None:
        date = datetime.now()

    for c in models.YandexDirectCampaign.objects.filter(is_active=True, banners__is_active=True):
        r, created = Report.objects.get_or_create(campaign=c, start_date=date, end_date=date)
        r.make_request()
