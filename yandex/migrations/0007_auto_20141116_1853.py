# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('yandex', '0006_auto_20141023_1246'),
    ]

    operations = [
        migrations.CreateModel(
            name='LatestYandexForecastItem',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('min', models.FloatField()),
                ('max', models.FloatField()),
                ('premium_min', models.FloatField()),
                ('premium_max', models.FloatField()),
                ('shows', models.IntegerField()),
                ('clicks', models.IntegerField()),
                ('first_place_clicks', models.IntegerField()),
                ('premium_clicks', models.IntegerField()),
                ('ctr', models.FloatField()),
                ('first_place_ctr', models.FloatField()),
                ('premium_ctr', models.FloatField()),
                ('phrase', models.ForeignKey(related_name=b'latest_forecasts', to='yandex.Phrase')),
                ('region', models.ForeignKey(to='yandex.YandexRegion')),
                ('report', models.ForeignKey(to='yandex.YandexForecastReport')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AlterUniqueTogether(
            name='latestyandexforecastitem',
            unique_together=set([('phrase', 'region')]),
        ),
    ]
