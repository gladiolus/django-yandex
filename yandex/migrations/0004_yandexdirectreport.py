# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('yandex', '0003_auto_20141021_1702'),
    ]

    operations = [
        migrations.CreateModel(
            name='YandexDirectReport',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('start_date', models.DateField()),
                ('end_date', models.DateField()),
                ('data', models.TextField(blank=True)),
                ('campaign', models.ForeignKey(related_name=b'reports', to='yandex.YandexDirectCampaign')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
