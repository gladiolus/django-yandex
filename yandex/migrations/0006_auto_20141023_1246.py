# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('xtemplate', '0002_template_name'),
        ('yandex', '0005_yandexdirectitem'),
    ]

    operations = [
        migrations.CreateModel(
            name='YandexDirectBannerTemplateRelation',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('field_name', models.CharField(max_length=255)),
                ('banner', models.ForeignKey(related_name=b'+', to='yandex.YandexDirectBanner')),
                ('template', models.ForeignKey(related_name=b'+', to='xtemplate.Template')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='yandexdirectbanner',
            name='links',
            field=models.ManyToManyField(related_name=b'banners', to='xtemplate.Link'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='yandexdirectbanner',
            name='templates',
            field=models.ManyToManyField(related_name=b'banners', through='yandex.YandexDirectBannerTemplateRelation', to='xtemplate.Template'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='yandexdirectbanner',
            name='external',
            field=models.BigIntegerField(default=0, unique=True),
        ),
        migrations.AlterField(
            model_name='yandexdirectcampaign',
            name='external',
            field=models.BigIntegerField(default=0, unique=True),
        ),
    ]
