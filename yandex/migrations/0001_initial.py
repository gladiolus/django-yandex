# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Phrase',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('text', models.TextField()),
                ('phrase', models.TextField(blank=True)),
                ('hash', models.CharField(max_length=32, blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='PhraseRelation',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('type', models.CharField(db_index=True, max_length=15, choices=[(b'search_also', 'Search also'), (b'search_with', 'Search with'), (b'suggestion', 'Suggestion')])),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('phrase_from', models.ForeignKey(related_name=b'rel_from', to='yandex.Phrase')),
                ('phrase_to', models.ForeignKey(related_name=b'rel_to', to='yandex.Phrase')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='YandexDirectBanner',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('external', models.IntegerField()),
                ('is_active', models.BooleanField(default=False)),
                ('status_show', models.BooleanField(default=False)),
                ('domain', models.CharField(max_length=255)),
                ('href', models.TextField()),
                ('title', models.CharField(max_length=33)),
                ('text', models.TextField()),
                ('raw', models.TextField()),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='YandexDirectCampaign',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('external', models.IntegerField(unique=True)),
                ('login', models.CharField(max_length=255)),
                ('name', models.CharField(max_length=255)),
                ('start_date', models.DateTimeField()),
                ('is_active', models.BooleanField(default=False)),
                ('raw', models.TextField()),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='YandexForecastItem',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('min', models.FloatField()),
                ('max', models.FloatField()),
                ('premium_min', models.FloatField()),
                ('premium_max', models.FloatField()),
                ('shows', models.IntegerField()),
                ('clicks', models.IntegerField()),
                ('first_place_clicks', models.IntegerField()),
                ('premium_clicks', models.IntegerField()),
                ('ctr', models.FloatField()),
                ('first_place_ctr', models.FloatField()),
                ('premium_ctr', models.FloatField()),
                ('phrase', models.ForeignKey(related_name=b'forecasts', to='yandex.Phrase')),
            ],
            options={
                'get_latest_by': 'created',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='YandexForecastReport',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('external', models.IntegerField(default=0)),
                ('phrases', models.ManyToManyField(to='yandex.Phrase')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='YandexForecastReportData',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('data', models.TextField()),
                ('report', models.OneToOneField(related_name=b'data', to='yandex.YandexForecastReport')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='YandexRegion',
            fields=[
                ('id', models.IntegerField(serialize=False, primary_key=True)),
                ('name', models.CharField(max_length=255)),
                ('name_ru', models.CharField(max_length=255)),
                ('type', models.CharField(db_index=True, max_length=30, blank=True)),
                ('parent', models.ForeignKey(related_name=b'children', blank=True, to='yandex.YandexRegion', null=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='YandexRubric',
            fields=[
                ('id', models.IntegerField(serialize=False, primary_key=True)),
                ('name', models.CharField(max_length=255)),
                ('name_ru', models.CharField(max_length=255)),
                ('full_name', models.CharField(max_length=255)),
                ('full_name_ru', models.CharField(max_length=255)),
                ('url', models.CharField(max_length=255)),
                ('checkable', models.BooleanField(default=False)),
                ('parent', models.ForeignKey(related_name=b'children', blank=True, to='yandex.YandexRubric', null=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='YandexWordstatReport',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('external', models.IntegerField(default=0)),
                ('phrases', models.ManyToManyField(to='yandex.Phrase')),
                ('region', models.ForeignKey(blank=True, to='yandex.YandexRegion', null=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='YandexWordstatReportData',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('data', models.TextField()),
                ('report', models.OneToOneField(related_name=b'data', to='yandex.YandexWordstatReport')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='YandexXMLRequest',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('phrase', models.ForeignKey(to='yandex.Phrase')),
                ('region', models.ForeignKey(to='yandex.YandexRegion', null=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='YandexXMLRequestPage',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('page', models.IntegerField(default=0)),
                ('data', models.TextField(blank=True)),
                ('request', models.ForeignKey(related_name=b'pages', to='yandex.YandexXMLRequest')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='yandexforecastreport',
            name='region',
            field=models.ForeignKey(blank=True, to='yandex.YandexRegion', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='yandexforecastitem',
            name='region',
            field=models.ForeignKey(to='yandex.YandexRegion'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='yandexforecastitem',
            name='report',
            field=models.ForeignKey(to='yandex.YandexForecastReport'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='yandexdirectbanner',
            name='campaign',
            field=models.ForeignKey(related_name=b'banners', to='yandex.YandexDirectCampaign'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='yandexdirectbanner',
            name='regions',
            field=models.ManyToManyField(related_name=b'banners', to='yandex.YandexRegion'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='phrase',
            name='related',
            field=models.ManyToManyField(related_name=b'+', through='yandex.PhraseRelation', to='yandex.Phrase'),
            preserve_default=True,
        ),
    ]
