# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('yandex', '0004_yandexdirectreport'),
    ]

    operations = [
        migrations.CreateModel(
            name='YandexDirectItem',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('clicks', models.IntegerField()),
                ('shows', models.IntegerField()),
                ('sum', models.DecimalField(max_digits=14, decimal_places=2)),
                ('clicks_search', models.IntegerField()),
                ('shows_search', models.IntegerField()),
                ('sum_search', models.DecimalField(max_digits=14, decimal_places=2)),
                ('clicks_context', models.IntegerField()),
                ('shows_context', models.IntegerField()),
                ('sum_context', models.DecimalField(max_digits=14, decimal_places=2)),
                ('banner', models.ForeignKey(to='yandex.YandexDirectBanner')),
                ('campaign', models.ForeignKey(to='yandex.YandexDirectCampaign')),
                ('phrase', models.ForeignKey(to='yandex.YandexDirectPhrase')),
                ('report', models.ForeignKey(to='yandex.YandexDirectReport')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
