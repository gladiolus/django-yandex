# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('yandex', '0002_auto_20141021_1620'),
    ]

    operations = [
        migrations.AlterField(
            model_name='yandexdirectbanner',
            name='external',
            field=models.BigIntegerField(unique=True),
        ),
        migrations.AlterField(
            model_name='yandexdirectcampaign',
            name='external',
            field=models.BigIntegerField(unique=True),
        ),
    ]
