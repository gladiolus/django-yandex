# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('yandex', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='YandexDirectPhrase',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('external', models.BigIntegerField(unique=True)),
                ('raw', models.TextField()),
                ('phrase', models.ForeignKey(to='yandex.Phrase')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='yandexdirectbanner',
            name='phrases',
            field=models.ManyToManyField(related_name=b'banners', to='yandex.YandexDirectPhrase'),
            preserve_default=True,
        ),
    ]
