from django.contrib import admin

from . import models


class YandexRegionAdmin(admin.ModelAdmin):
    list_display = ('name', 'name_ru', 'parent', 'type', )
    search_fields = ('name', 'name_ru')
    list_filter = ('type',)


def make_request(modeladmin, request, queryset):
    for instance in queryset:
        instance.make_request()


class YandexReportAdmin(admin.ModelAdmin):
    actions = [make_request]


class PhraseRelationAdmin(admin.ModelAdmin):
    list_display = ('phrase_from', 'phrase_to', 'type', 'created', )
    list_filter = ('type', )
    search_fields = ('phrase_from__phrase', )

    def get_search_results(self, request, queryset, search_term):
        if search_term:
            queryset = queryset.filter(phrase_from__hash=models.Phrase.calc_hash(search_term))
        return super(PhraseRelationAdmin, self).get_search_results(request, queryset, search_term)


class YandexXmlRequestAdmin(admin.ModelAdmin):
    list_display = ('phrase', 'region', 'created', )


class YandexXmlRequestPageAdmin(admin.ModelAdmin):
    list_display = ('get_phrase', 'page', )

    def get_phrase(self, obj):
        return obj.request.phrase
    get_phrase.short_description = u'Request'
    get_phrase.admin_order_field = 'request__phrase__phrase'


class YandexWordstatReportAdmin(YandexReportAdmin):
    list_display = ('get_phrases', 'created', 'region', 'external', )

    def get_phrases(self, obj):
        return u', '.join([phrase.phrase for phrase in obj.phrases.all()])
    get_phrases.short_description = u'Phrases'


class YandexWordstatReportDataAdmin(admin.ModelAdmin):
    list_display = ('get_phrases', )

    def get_phrases(self, obj):
        return u', '.join([phrase.phrase for phrase in obj.report.phrases.all()])
    get_phrases.short_description = u'Report'


class YandexForecastReportAdmin(YandexReportAdmin):
    list_display = ('created', 'region', 'external', )


class ShowsFilter(admin.SimpleListFilter):
    template = "shows_filter.html"
    title = u'Shows'
    parameter_name = 'shows'

    def lookups(self, request, model_admin):
        shows = request.GET.get('shows')
        if shows:
            shows_from, shows_to = shows.split(',')
        else:
            shows_from = shows_to = ''
        self.shows_from = shows_from
        self.shows_to = shows_to
        return [(shows_from, 'from'), (shows_to, 'to')]

    def queryset(self, request, queryset):
        if self.value():
            if self.shows_from:
                queryset = queryset.filter(shows__gte=self.shows_from)
            if self.shows_to:
                queryset = queryset.filter(shows__lt=self.shows_to)
        return queryset

    def choices(self, cl):
        import urlparse
        _choices = super(ShowsFilter, self).choices(cl)
        _choices = list(_choices)[1:]
        values = {'from': self.shows_from, 'to': self.shows_to}

        query_string = _choices[0]['query_string'][1:]
        params = urlparse.parse_qs(query_string)
        params = {key: value[0] for key, value in params.items()}

        if 'shows' in params:
            del params['shows']

        query = '?' + '&'.join(['{}={}'.format(key, value) for key, value in params.items()])

        for choice in _choices:
            choice['value'] = values[choice['display']]
            choice['query'] = query

        return _choices


class YandexForecastReportItemAdmin(admin.ModelAdmin):
    list_display = ('phrase', 'region', 'created', 'min', 'max', 'premium_min', 'premium_max', 'shows', 'clicks',
                    'first_place_clicks', 'premium_clicks', 'ctr', 'first_place_ctr', 'premium_ctr', )
    search_fields = ('phrase__phrase', )
    list_filter = (ShowsFilter, )

    def get_search_results(self, request, queryset, search_term):
        if search_term:
            queryset = queryset.filter(phrase__hash=models.Phrase.calc_hash(search_term))
        return super(YandexForecastReportItemAdmin, self).get_search_results(request, queryset, search_term)


class YandexDirectCampaignAdmin(admin.ModelAdmin):
    list_display = ('external', 'name', 'login', 'start_date', 'is_active')
    list_filter = ('is_active', )


class YandexDirectBannerAdmin(admin.ModelAdmin):
    list_display = ('external', 'title', 'is_active', 'status_show')
    list_filter = ('is_active', 'status_show')


class YandexDirectReportAdmin(admin.ModelAdmin):
    list_display = ('campaign', 'start_date', 'end_date')


class YandexDirectItemAdmin(admin.ModelAdmin):
    list_display = ('campaign', 'banner', 'phrase', 'clicks', 'shows', 'sum', 'clicks_search', 'shows_search',
                    'sum_search', 'clicks_context', 'shows_context', 'sum_context')


admin.site.register(models.YandexRegion, YandexRegionAdmin)
admin.site.register(models.YandexRubric)
admin.site.register(models.Phrase)
admin.site.register(models.PhraseRelation, PhraseRelationAdmin)
admin.site.register(models.YandexXMLRequest, YandexXmlRequestAdmin)
admin.site.register(models.YandexXMLRequestPage, YandexXmlRequestPageAdmin)
admin.site.register(models.YandexWordstatReport, YandexWordstatReportAdmin)
admin.site.register(models.YandexWordstatReportData, YandexWordstatReportDataAdmin)
admin.site.register(models.YandexForecastReport, YandexForecastReportAdmin)
admin.site.register(models.YandexForecastReportData)
admin.site.register(models.YandexForecastItem, YandexForecastReportItemAdmin)
admin.site.register(models.LatestYandexForecastItem, YandexForecastReportItemAdmin)
admin.site.register(models.YandexDirectCampaign, YandexDirectCampaignAdmin)
admin.site.register(models.YandexDirectBanner, YandexDirectBannerAdmin)
admin.site.register(models.YandexDirectPhrase)
admin.site.register(models.YandexDirectReport, YandexDirectReportAdmin)
admin.site.register(models.YandexDirectItem, YandexDirectItemAdmin)
