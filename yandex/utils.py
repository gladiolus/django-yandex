import itertools
import json
import logging
import re
import requests

from django.conf import settings
from django.db import transaction
from xtemplate.models import Link


class YandexError(requests.exceptions.RequestException):
    def __init__(self, *args, **kwargs):
        super(YandexError, self).__init__(*args, **kwargs)
        self.yandex_code = None

        if self.response:
            self.yandex_code = self.response.json().get('error_code')
            self.yandex_queue_is_full = (self.yandex_code == 31)


logger = logging.getLogger(__name__)


def split_seq(iterable, size):
    it = iter(iterable)
    item = list(itertools.islice(it, size))
    while item:
        yield item
        item = list(itertools.islice(it, size))


def do_yandex_direct_request(method, param=None, raise_on_errors=False):

    def dump_data(data):
        return json.dumps(data, ensure_ascii=False).encode('utf-8')

    if settings.YANDEX_DIRECT_CERT_PATH and settings.YANDEX_DIRECT_KEY_PATH:
        data = dict(method=method, param=param)
        r = requests.post(settings.YANDEX_DIRECT_URL, data=dump_data(data), timeout=settings.YANDEX_TIMEOUT,
                          cert=(settings.YANDEX_DIRECT_CERT_PATH, settings.YANDEX_DIRECT_KEY_PATH))
    elif settings.YANDEX_DIRECT_TOKEN:
        data = dict(method= method, token=settings.YANDEX_DIRECT_TOKEN, param=param)
        r = requests.post(settings.YANDEX_DIRECT_URL, data=dump_data(data), timeout=settings.YANDEX_TIMEOUT)

    if raise_on_errors:
        r.raise_for_status()

    if r.status_code != requests.codes.ok:
        logger.error("Yandex direct error code=%s, method=%s: %s, param=%s",
                     r.status_code, method, r.content, param)

    elif 'error_code' in r.json():
        msg = u"Yandex direct error method={}: {}, param={}".format(method, r.content, param)
        if raise_on_errors:
            raise YandexError(msg, response=r)
        logger.error(msg)

    return r


def split_yandex_phrases(text):
    result = map(unicode.strip, re.split(r'[,\n\t]', unicode(text), re.U))
    return filter(len, result)


@transaction.commit_manually
def create_yandex_wordstat_report(qs, region):
    from .models import YandexWordstatReport
    for i, ph in enumerate(split_seq(qs, settings.YANDEX_WORDSTAT_PHRASE_LIMIT)):
        try:
            report = YandexWordstatReport(region=region)
            report.save()
            for p in ph:
                report.phrases.add(p)
        except:
            transaction.rollback()
            raise
        else:
            transaction.commit()
            countdown = settings.YANDEX_COUNTDOWN*i
            report.make_request(countdown=countdown)


@transaction.commit_manually
def create_yandex_forecast_report(qs, region):
    from .models import YandexForecastReport
    for i, ph in enumerate(split_seq(qs, settings.YANDEX_FORECAST_PHRASE_LIMIT)):
        try:
            report = YandexForecastReport(region=region)
            report.save()
            for p in ph:
                report.phrases.add(p)
        except:
            transaction.rollback()
            raise
        else:
            transaction.commit()
            countdown = settings.YANDEX_COUNTDOWN*i
            report.make_request(countdown=countdown)


@transaction.commit_manually
def create_yandex_xml_requests(qs, region):
    from .models import YandexXMLRequest
    from .tasks import do_yandex_xml_request
    for i, ph in enumerate(qs):
        try:
            request = YandexXMLRequest(phrase=ph, region=region)
            request.save()
        except:
            transaction.rollback()
            raise
        else:
            transaction.commit()
            do_yandex_xml_request.apply_async((request.pk,), countdown=i*settings.YANDEX_XML_MAX_PAGES)


def create_banner(template_names, campaign, context):
    from .models import YandexDirectBanner, YandexDirectBannerTemplateRelation
    from xtemplate.models import Template

    template_links = []
    templates = {}
    rendered = {}
    for field_name in ('href', 'title', 'text'):
        template = Template.objects.get(name=template_names[field_name])
        text, links = template.render(**context)

        templates[field_name] = template
        rendered[field_name] = text
        template_links.extend(links)

    print rendered['title']
    banner = YandexDirectBanner(domain=settings.YANDEX_BANNER_DOMAIN,
                                href=rendered['href'], title=rendered['title'],
                                text=rendered['text'], campaign=campaign)
    banner.save()

    for link in template_links:
        try:
            link = Link.objects.get(object=link.object, context=link.context)
        except Link.DoesNotExist:
            link.save()

        banner.links.add(link)

    for field_name, template in templates.items():
        YandexDirectBannerTemplateRelation(banner=banner, template=template,
                                           field_name=field_name).save()

    return banner


def merge_dicts(dict1, dict2):
    for k in set(dict1.keys()).union(dict2.keys()):
        if k in dict1 and k in dict2 and isinstance(dict1[k], dict) and isinstance(dict2[k], dict):
            yield (k, dict(merge_dicts(dict1[k], dict2[k])))
        elif k in dict2:
            yield (k, dict2[k])
        else:
            yield (k, dict1[k])
