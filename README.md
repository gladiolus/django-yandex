settings
========

    YANDEX_TIMEOUT = 10
    
    YANDEX_XML_URL = "http://xmlsearch.yandex.ru/xmlsearch"
    
    YANDEX_XML_USER = ""
    
    YANDEX_XML_KEY = ""
    
    YANDEX_XML_PROXIES = None
    
    YANDEX_XML_MAX_PAGES = 10
    
    YANDEX_DIRECT_URL = "https://api.direct.yandex.ru/v4/json/"
    
    YANDEX_DIRECT_TOKEN = ""
    
    YANDEX_DIRECT_CERT_PATH = ""
    
    YANDEX_DIRECT_KEY_PATH = ""
    
    YANDEX_WORDSTAT_PHRASE_LIMIT = 10
    
    YANDEX_FORECAST_PHRASE_LIMIT = 100
    
    YANDEX_COUNTDOWN = 30
