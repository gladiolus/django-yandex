#!/usr/bin/env python

from setuptools import setup


setup(
    name="django-yandex",
    version="0.1.0",
    packages=[
        'yandex',
        'xtemplate',
    ],
    package_data = {
        'yandex': ['templates/*.html'],
    },
    zip_safe=False,
    install_requires=[
        "requests",
        "django >= 1.7",
        "celery",
    ]
)
