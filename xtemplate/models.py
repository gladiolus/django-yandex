from django.db import models
from django.contrib.contenttypes.models import ContentType
from django.template import Context as DjangoContext, Template as DjangoTemplate


class Context(models.Model):
    name = models.CharField(max_length=255, db_index=True)
    content_type = models.ForeignKey(ContentType)
    template = models.ForeignKey('Template', related_name='context')

    def __unicode__(self):
        return u'{} - {}'.format(self.content_type, self.name)


class Template(models.Model):
    name = models.CharField(max_length=255, unique=True)
    content = models.TextField()

    def __unicode__(self):
        return self.name

    def render(self, **kwargs):
        links = []

        contexts = {c.name: c for c in self.context.all()}
        for name, obj in kwargs.items():
            c = contexts.get(name)
            assert(c is not None)
            assert(c.content_type == ContentType.objects.get_for_model(obj))
            links.append(Link(object=obj.pk, context=c))

        template = DjangoTemplate(self.content)
        text = template.render(DjangoContext(kwargs))
        return text, links


class Link(models.Model):
    object = models.PositiveIntegerField(db_index=True)
    context = models.ForeignKey(Context)

    class Meta:
        unique_together = (('object', 'context'),)

    def __unicode__(self):
        return u'{}'.format(self.object)
