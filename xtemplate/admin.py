from django.contrib import admin

from .models import Context, Link, Template


class ContextAdmin(admin.ModelAdmin):
    save_as = True
    list_display = ('name', 'type', 'template')
    search_fields = ('name', 'template__name')
    list_filter = ['template']

    def type(self, obj):
        return u"{}.{}".format(obj.content_type.app_label, obj.content_type.name)

admin.site.register(Context, ContextAdmin)
admin.site.register(Link)
admin.site.register(Template)
